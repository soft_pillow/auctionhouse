class Buyer:

    def __init__(self, name):
        self.name = name
        self.__total_bids = 0 # useful for preferred buyer

    def add_bids(self):
        self.__total_bids += 1

    def remove_bids(self):
        self.__total_bids -= 0

    def __repr__(self):
        return "{}".format(self.name)

