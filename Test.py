

from Auction import Auction
from Buyer import Buyer
from Seller import Seller
from Platform import Platform

s1= Seller("1")
s2= Seller("2")

b1= Buyer("1")
b2= Buyer("2")
b3= Buyer("3")


p = Platform()

a1 = p.add_auction(10, 50, 1, s1)

a1.add_bid(b1, 17)
a1.add_bid(b2, 19)
a1.add_bid(b2, 19)
a1.add_bid(b3, 19)

a1.close()
a1.profit()

a2 = p.add_auction(5, 20, 2, s2)
a2.add_bid(b3,25)
a2.add_bid(b2, 5)
a2.withdraw_bid(b2)

a2.close()
a2.profit()


