from uuid import uuid4
from Auction import Auction

class Platform:

    auctions = []
    name = "FooBar"

    def __init__(self):
        pass

    @classmethod
    def add_auction(cls, low, high, cost, seller):
        id = uuid4().hex
        new_acution = Auction(id, low, high, cost, seller)
        cls.auctions.append(new_acution)
        return new_acution
