class Seller:

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return "{}".format(self.name)
