

class Auction:

    def __init__(self, id, low, high, cost, seller, live=True):

        self.id = id
        self.low = low
        self.high = high
        self.cost = cost
        self.seller = seller
        self.live = live
        self._bids = {}  # { 10 : [b1], 20:[b2] }
        self._bidders = set()
        self._participation_cost = 0

    def add_bid(self, buyer, amount):

        if not self.live:
            return

        if self.low <= amount <= self.high:

            if amount in self._bids:
                self._bids[amount].add(buyer)
            else:
                self._bids[amount] = set()
                self._bids[amount].add(buyer)

            if buyer not in self._bidders:
                self._participation_cost += self.cost

            self._bidders.add(buyer)
            buyer.add_bids()

    def withdraw_bid(self, buyer):

        if not self.live:
            return

        if buyer not in self._bidders:
            return

        all_bids = [ buyers for amount, buyers in self._bids.items()]

        for bidders in all_bids:
            bidders.remove(buyer)

        self._bidders.remove(buyer)
        buyer.remove_bids()

    def _unique_bid(self):
        unique = [ amt for amt, bidders in self._bids.items() if len(bidders) == 1 ]
        unique_amount = max(unique) if len(unique) > 0 else 0
        return unique_amount

    def profit(self):

        max_unique = self._unique_bid()

        if max_unique:
            return max_unique + (self._participation_cost *0.2) - ((self.high+self.low)/2.0)
        else:
            return (self._participation_cost * 0.2) - ((self.high + self.low)/2.0)

    def close(self):
        self.live = False
        max_unique = self._unique_bid()

        if max_unique:
            print ("Winner is {}".format(self._bids[max_unique]))
        else:
            print ("No winners!")


    def __repr__(self):
        return "{}-{}".format(self.id, self.live)